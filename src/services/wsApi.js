import axios from 'axios'

const $axios = axios

class WsApi {
  constructor () {
    let basePath = 'http://wiselabapi.wiselab.io/wiselab/'
    // let basePath = 'http://69585125.ngrok.io/wiselab'
    $axios.defaults.baseURL = basePath
    $axios.defaults.headers['Content-Type'] = 'application/json'
  }

  get $axios () {
    return $axios
  }
}

export default new WsApi()