import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store';
import './registerServiceWorker'
import  './plugins/vuetify'
import axios from 'axios'
import './styles/common.css'
import wsapi from  './services/wsApi'
import '@mdi/font/css/materialdesignicons.css'
import FlagIcon from 'vue-flag-icon'
import i18n from './i18n'
// import store from './store/index'

Vue.use(FlagIcon)

Vue.config.productionTip = false

Vue.prototype.$axios = axios

const token = window.sessionStorage.getItem("token")
console.log("TOKEN: "+token)
if (token) { 
  wsapi.$axios.defaults.headers.common['Authorization'] = `Bearer ${token}`
}

new Vue({
  router,
  store,
  iconfont: 'mdi',
  i18n,
  render: h => h(App)
}).$mount('#app')
