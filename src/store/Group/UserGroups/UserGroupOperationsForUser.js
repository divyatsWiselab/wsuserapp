import wsapi from  '../../../services/wsApi'
 

const state = {
    userListFromUsersGroup: []
}

// getters
const getters = {
  getUserListFromUserGroup(state){
    return state.userListFromUsersGroup
  }
}

// actions
const actions = {
  ADD_USER_LIST_TO_USERGROUP({ commit }, dataToSend) {
    return wsapi.$axios.post(`usergroups/${dataToSend.userGroupId}/user` , dataToSend.data)
    .then(response => {
      return response
    })
  },
  GET_USER_LIST_FROM_USERGROUP_BY_ID ({ commit }, userGroupId) {
    var pagesize = 100
    //ASK ALL FOR A MOMENT
    return wsapi.$axios.get(`usergroups/${userGroupId}/user?page=0&size=${pagesize}`)
      .then(response => {
        commit('updateUserListFromUserGroup' , response.data.data.users)
        return response
      })
  },
}

// mutations
const mutations = {
    updateUserListFromUserGroup: (state, userListFromUsersGroup) => {
        state.userListFromUsersGroup = userListFromUsersGroup
    }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}