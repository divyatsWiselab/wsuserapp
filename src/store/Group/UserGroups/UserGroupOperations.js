import wsapi from  '../../../services/wsApi'
 

const state = {
    userGroupList: [],
    selectedUserGroup: {
      itemType: "USERGROUP",
      itemTittle: "Nutzergruppe",
      path: "/UserGroup",
      data: {}
    }
}

// getters
const getters = {
      getUserGroupList(state){
        return state.userGroupList
      },
      getSelectedUserGroup(state){
        return state.selectedUserGroup
      }
}

// actions
const actions = {
  ADD_USERGROUP({commit}, inputData) {
    return wsapi.$axios.post('usergroups' , inputData)
    .then(response => {
      return response
    })
  },
  
  DELETE_USERGROUP_BY_ID({commit}, userGroupId) {
    return wsapi.$axios.delete(`usergroups/${userGroupId}`)
    .then(response => {
      return response
    })
  },

  GET_USERGROUP_LIST ( {commit} ) {
    return wsapi.$axios.get('usergroups'  )
      .then(response => {
        commit('updateUserGroupList' , response.data.data.groups)
        return response
      })
  },
  GET_USERGROUP_BY_ID ( {commit}, userGroupId) {
    return wsapi.$axios.get(`usergroups/${userGroupId}`)
      .then(response => {
        commit('updateUserGroupList' , response.data.data.groups[0])
        return response
      })
  },
  UPDATE_USERGROUP (commit, dataToSend) {
    return wsapi.$axios.patch(`/usergroups/${dataToSend.id}`, dataToSend.data )
      .then(response => {
        return response
      })
  },
  SELECT_USERGROUP({commit}, selectedUserGroup)  {
    commit('updateSelectedUserGroup',selectedUserGroup)
  },

  // ADD_USER_SUBGROUP(commit, inputData) {
  //   return wsapi.$axios.post('usergroups/' + inputData.userGroupId +'/subgroups', inputData.newData).then(response => {
  //     return response.data
  //   })
  //   .catch(err => { console.log(err.response.data.errors)
  //     return err.response.data
  //   })
  //   },
     
  //   GET_USER_SUB_GROUPS ({ commit }, userGroupId) {
  //       return wsapi.$axios.get('usergroups/' + userGroupId + 'subgroups'  )
  //       .then(response => {
  //           commit('wsUpdateuserSUBGroupsList' , response.data.data.groups)
  //           console.log(state.userGroupList)
  //       })
  //       .catch(err => {          
  //           console.error(err)
  //       })
  //   },  
}

// mutations
const mutations = {
  updateUserGroupList: (state, userGroupList) =>{
    state.userGroupList = userGroupList
  },
  updateSelectedUserGroup: (state, selectedUserGroup) =>{
    state.selectedUserGroup.data = selectedUserGroup
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}