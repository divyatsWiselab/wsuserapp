import wsapi from  '../../../services/wsApi'
 

const state = {
    adminListFromAdmingroup: [],
}

// getters
const getters = {
  getAdminListFromAdminGroup(state){
    return state.adminListFromAdmingroup
  }
}

// actions
const actions = {
  ADD_ADMIN_LIST_TO_ADMINGROUP( {commit} ,dataToSend) {
      return wsapi.$axios.post(`admingroups/${dataToSend.adminGroupId}/admins`, dataToSend.data)
      .then(response => {
        return response
      })
  },
  
  GET_ADMIN_LIST_FROM_ADMINGROUP_BY_ID ({ commit }, adminGroupId) {
    return wsapi.$axios.get(`admingroups/${adminGroupId}/admins`)
      .then(response => {
        commit('updateAdminListFromAdminGroup' , response.data.data.admins)
        return response
      })
  },

  GET_SINGLE_ADMIN_FROM_ADMINGROUP ({ commit }, dataToSend) {
    return wsapi.$axios.get(`/admingroups/${dataToSend.adminGroupId}/admins/${dataToSend.adminId}`)
      .then(response => {
        //WE STILL HAVEN'T USE THIS YET.
        return response
      })
  }
}

// mutations
const mutations = {
    updateAdminListFromAdminGroup: (state, adminListFromAdmingroup) =>{
      state.adminListFromAdmingroup = adminListFromAdmingroup
    }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}