import wsapi from  '../../../services/wsApi'
 

const state = {
    adminGroupsList: [],
    selectedAdminGroup: {
      itemType: "ADMINGROUP",
      itemTittle: "Admingruppe",
      path: "/AdminGroup",
      data: {}
    }
}

// getters
const getters = {
    getAdminGroupsList(state){
      return state.adminGroupsList
    },
    getSelectedAdminGroup(state){
      return state.selectedAdminGroup
    }
}

// actions
const actions = {
  ADD_ADMINGROUP( {commit}, inputData) {
    return wsapi.$axios.post('admingroups' , inputData)
    .then(response => {
      return response
    })
  },
  DELETE_ADMINGROUP_BY_ID({commit}, adminGroupId) {
    return wsapi.$axios.delete(`admingroups/${adminGroupId}`)
    .then(response => {
      return response
    })
  },
  GET_ADMINGROUP_LIST ({ commit }) {
    return wsapi.$axios.get('admingroups'  )
      .then(response => {
        commit('updateAdminGroupsList' , response.data.data.groups)
        return response
      })
  },

  GET_ADMINGROUP_BY_ID ( {commit}, adminGroupId) {
    return wsapi.$axios.get(`admingroups/${adminGroupId}`)
      .then(response => {
        commit('updateAdminGroupsList',response.data.data)
        return response
      })
  },
  UPDATE_ADMINGROUP ( {commit}, dataToSend) {
    return wsapi.$axios.patch(`/admingroups/${dataToSend.id}`, dataToSend.data )
      .then(response => {
        return response
      })
  },
  SELECT_ADMINGROUP({commit}, selectedAdminGroup)  {
    commit('updateSelectedAdminGroup',selectedAdminGroup)
  },
//   ADD_ADMIN_SUBGROUP( inputData) {
//     return wsapi.$axios.post('admingroups/' + inputData.adminGroupId +'/subgroups', inputData.newData).then(response => {
//       return response.data
//     })
//     .catch(err => { console.log(err.response.data.errors)
//       return err.response.data
//     })
// },
     
//     GET_ADMIN_SUBGROUPS ({ commit }, adminGroupId) {
//         return wsapi.$axios.get('admingroups/' + adminGroupId + 'subgroups'  )
//         .then(response => {
//             commit('wsUpdateAdminSUBGroupsAdminList' , response.data.data.groups)
//             console.log(state.adminGroupsAdminList)
//         })
//         .catch(err => {          
//             console.error(err)
//         })
//     },    
    UPDATE_PARENTGROUP_OF_ADMINGROUP ( newDataObj) {
        return wsapi.$axios.patch(`/admingroups/${newDataObj.adminGroupId}/parent`, newDataObj.newData )
          .then(response => {
            return response
          })
      },
}

// mutations
const mutations = {
    updateAdminGroupsList: (state, adminGroupsList) => {
        state.adminGroupsList = adminGroupsList
    },
    updateSelectedAdminGroup: (state, selectedAdminGroup) =>{
      state.selectedAdminGroup.data = selectedAdminGroup
    }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}