
import wsapi from  '../../services/wsApi'
 

const state = {
  catalogSubGroupList: [],
  selectedCatalogSubGroup: {
    itemType: "CATALOGSUBGROUP",
    itemTittle: "Kataloguntergruppe",
    path: "/Authorentool_CatalogSubgroup",
    data: {}
  }
}

// getters
const getters = {
    getCatalogSubGroupList(state){
      return state.catalogSubGroupList
    },
    getSelectedCatalogSubGroup(state) {
      return state.selectedCatalogSubGroup
    }
}

// actions
const actions = {
  ADD_CATALOGSUBGROUP({commit}, dataToSend) {
    return wsapi.$axios.post(`cataloggroups/${dataToSend.catalogGroupId}/subgroups`, dataToSend.data)
    .then(response => {
      return response
    })
  },
  
  DELETE_CATALOGSUBGROUP({commit}, dataToSend) {
  return wsapi.$axios.delete(`cataloggroups/${dataToSend.catalogGroupId}/subgroups/${dataToSend.catalogSubgroupId}`)
  .then(response => {
    return response
  })
  },

  GET_CATALOGSUBGROUP_LIST({commit}, catalogGroup) {
    return wsapi.$axios.get(`cataloggroups/${catalogGroup.id}/subgroups`)
    .then(response => {
      commit('updateCatalogSubGroupList' , response.data.data.subGroups)
      return response
    })
  },

  GET_SINGLE_CATALOGSUBGROUP({commit}, dataToSend)  {
    return wsapi.$axios.get(`cataloggroups/${dataToSend.catalogGroupId}/subgroups/${dataToSend.catalogSubgroupId}`)
    .then(response => {
      commit('updateCatalogSubGroupList' , response.data.data)
      return response
    })
  },

  SELECT_CATALOGSUBGROUP({commit}, selectedCatalogSubGroup)  {
    commit('updateSelectedCatalogSubgroup',selectedCatalogSubGroup)
  },

  UPDATE_CATALOGSUBGROUP( {commit}, dataToSend) {
    return wsapi.$axios.patch(`/cataloggroups/${dataToSend.catalogGroupId}/subgroups/${dataToSend.catalogSubgroupId}`, dataToSend.newData )
    .then(response => {
      return response
    })
  }
}

// mutations
const mutations = {
    updateCatalogSubGroupList: (state, catalogSubGroupList) =>{
      state.catalogSubGroupList = catalogSubGroupList
    },
    updateSelectedCatalogSubgroup: (state, selectedCatalogSubGroup) =>{
      state.selectedCatalogSubGroup.data = selectedCatalogSubGroup
    }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}