import wsapi from  '../../../services/wsApi'


//HAVEN'T KNOW WHAT THIS STORE FOR



const state = {
    catalogSubGroupAdminList: []
}

// getters
const getters = {
  catalogSubGroupAdminList(state){
    return state.catalogSubGroupAdminList
  }
}

// actions
const actions = {
     ADD_ADMIN_TO_CATALOGSUBGROUP( dataToSend) {
      return wsapi.$axios.post(`cataloggroups/${dataToSend.catalogGroupId}/subgroups/${inputData.subGroupId}/admins`, dataToSend.data)
      .then(response => {
        return response
      })
  },
  
   DELETE_ADMIN_FROM_CATALOGSUBGROUP(dataToSend) {
    return wsapi.$axios.delete(`cataloggroups/${dataToSend.catalogGroupId}/subgroups/${dataToSend.subGroupId}/admins`)
    .then(response => {
      return response
    })
  },

  GET_ADMIN_LIST_FROM_CATALOGSUBGROUP_BY_ID ({ commit }, catalogGroupId) {
    return wsapi.$axios.get(`cataloggroups/${catalogGroupId}/admingroups`)
      .then(response => {
        commit('updateCatalogGroupAdminList' , response.data.data.groups)
        return response
      })
  }
}

// mutations
const mutations = {
  updateCatalogGroupAdminList: (state, catalogSubGroupAdminList) =>{
    state.catalogSubGroupAdminList = catalogSubGroupAdminList
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}