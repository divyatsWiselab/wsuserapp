/*
This index.js file giving accessess to all store
file in the subfolder to outside
*/

/* eslint-disable no-unused-vars */
import Vue from 'vue'
import Vuex from 'vuex'

import General from './General/LoginOperations'
import User from './User/UserOperations'
import Catalog from './Catalog/Catalog'
import Admin from './Admin/AdminOperations'
import AdminPermission from './Permission/Authoring/AdminPermission'
import CGOperations from './CatalogGroup/CGOperations'
import CSGOperations from './CatalogSubGroup/CSGOperations'
import CGHistory from './CatalogGroup/CGHistory'
import AdminInCG from './CatalogGroup/AdminInCG'
import AdminGroupsOp from './Group/AdminGroups/AdminGroupOperation'
import AdminGroupsOpforAdmin from './Group/AdminGroups/AdminGroupOperationsForAdmin'
import UserGroupsOp from './Group/UserGroups/UserGroupOperations'
import UserGroupsOpforUser from './Group/UserGroups/UserGroupOperationsForUser'
import Routing from './Routing'
import AppLanguage from './GlobalConfig/AppLanguage'

Vue.use(Vuex)

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation
 */

export default function (/* { ssrContext } */) {
  const store = new Vuex.Store({
    modules: {
      Routing,
      General,  
      Admin,
      AdminPermission,
      User,
      Catalog,
      CGOperations,
      CSGOperations,
      CGHistory,
      AdminInCG,
      AdminGroupsOp,
      AdminGroupsOpforAdmin,
      UserGroupsOp,
      UserGroupsOpforUser,
      AppLanguage
    } 
  })

  return store
}
