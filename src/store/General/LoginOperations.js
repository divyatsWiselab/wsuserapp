import wsapi from  '../../services/wsApi'
 

const state = {
  accessToken : sessionStorage.getItem("token"),
  userRole :'',
  loginUserDetails : {}
}

// getters
const getters = {
  accesstoken (state) {
    return state.accessToken
  },
  getUserRole(state){
    return state.userRole
  },
  getLoginUserDetails(state){
    return state.loginUserDetails
  }
}

// actions
const actions = {
    LOGIN  ({commit, getters}, loginData) {
      return wsapi.$axios.post('auth/login', loginData)
      .then(response => {
        commit('updateToken', response.data.data.accessToken)
        commit('updateUserRole', response.data.data.user.role)
        commit('updateLoginUserDetails', response.data.data.user)
        wsapi.$axios.defaults.headers.common['Authorization'] = `Bearer ${getters.accesstoken}`
        return response
      })
    },
    
    FORGOT_PASSWORD ( emailId) {
      return wsapi.$axios.post('auth/forgotpassword', emailId)
      .then(response => {      
        return response
      })
    },
    
    SET_NEW_PASSWORD ( inputData) {
      return wsapi.$axios.post('auth/password', inputData)
      .then(response => {      
        return response
      })
    },
    
    ACTIVATE_NEW_PASSWORD ( inputData) {
      return wsapi.$axios.post('/activate', inputData)
      .then(response => {      
        return response
      })
    },

    LOGOUT ( {commit} ) {
    commit('updateToken', null)
      delete wsapi.$axios.defaults.headers.common['Authorization']
      return null
    }   
}

// mutations
const mutations = {
  updateToken: (state, newvalue)  => {
    state.accessToken = newvalue
  },
  updateUserRole: (state, newvalue)  => {
    state.userRole = newvalue
  },
  updateLoginUserDetails : (state, newvalue) => {
    state.loginUserDetails = newvalue
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}