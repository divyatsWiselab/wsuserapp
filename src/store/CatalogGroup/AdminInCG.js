import wsapi from  '../../services/wsApi'
 

//HAVEN'T KNOW WHAT THIS STORE FOR

const state = {
    catalogGroupAdminList: []
}

// getters
const getters = {
  catalogGroupAdminList(state){
    return state.catalogGroupAdminList
  }
}

// actions
const actions = {
  ADD_ADMIN_TO_CATALOG_GROUP( dataToSend) {
    return wsapi.$axios.post(`cataloggroups/${dataToSend.adminId}/admins`, dataToSend.data)
    .then(response => {
      return response
    })
  },
  
   DELETE_ADMIN_FROM_CATALOGGROUP_BY_ID(catalogGroupId) {
    return wsapi.$axios.delete(`cataloggroups/${catalogGroupId}/admins`)
    .then(response => {
      return response
    })
  },

  GET_CATALOGGROUP_LIST_FROM_ADMIN_BY_ID ({ commit }, catalogGroupID) {
    return wsapi.$axios.get(`cataloggroups/${catalogGroupID}/admingroups`)
      .then(response => {
        commit('updateCatalogGroupAdminList' , response.data.data.groups)
        return response
      })
  }
}

// mutations
const mutations = {
    updateCatalogGroupAdminList: (state, catalogGroupAdminList) => {
      state.catalogGroupAdminList = catalogGroupAdminList
    }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}