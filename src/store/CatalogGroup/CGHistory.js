import wsapi from  '../../services/wsApi'
 

const state = {
    catalogGroupHistory: []
}

// getters
const getters = {
    getCatalogGroupHistory(state){
        return state.catalogGroupHistory
      }
}

// actions
const actions = {
  GET_ALL_ADMINGROUPS ({ commit }, catalogGroupID) {
    return wsapi.$axios.get(`cataloggroups/${catalogGroupID}/history`)
    .then(response => {
      commit('updateCatalogGroupHistory' , response.data.data.groups)
    })
  }
}

// mutations
const mutations = {
  updateCatalogGroupHistory: (state, catalogGroupHistory) => {
    state.catalogGroupHistory = catalogGroupHistory
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}