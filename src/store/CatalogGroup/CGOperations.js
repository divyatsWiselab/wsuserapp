import wsapi from  '../../services/wsApi'
 

const state = {
  catalogGroupList: [],
  selectedCatalogGroup: {
    itemType: "CATALOGGROUP",
    itemTittle: "Kataloggruppe",
    path: "/Authorentool_CatalogGroup",
    data: {}
  }
}

// getters
const getters = {
    getCatalogGroupList(state){
        return state.catalogGroupList
    },
    getSelectedCatalogGroup(state) {
      return state.selectedCatalogGroup
    }
}

// actions
const actions = {
    ADD_CATALOGGROUP({commit},dataToSend) {
        return wsapi.$axios.post('cataloggroups', dataToSend)
        .then(response => {
          return response
        })
  },
  
   DELETE_CATALOGGROUP_BY_ID({commit}, catalogGroupId) {
    return wsapi.$axios.delete(`cataloggroups/${catalogGroupId}`)
    .then(response => {
      return response
    })
  },

  GET_CATALOGGROUP_LIST   ({ commit }) {
    return wsapi.$axios.get('cataloggroups?page=0&size=20&sort=createdAt,desc')
      .then(response => {
        commit('updateCatalogGroupList' , response.data.data.groups)
        return response
      })
  },

  GET_SINGLE_CATALOGGROUP_BY_ID( {commit}, catalogGroupId)  {
    return wsapi.$axios.get(`/cataloggroups/${catalogGroupId}`)
      .then(response => {
        commit('updateCatalogGroupList' , response.data.data)
        return response
      })
  },

  SELECT_CATALOGGROUP( {commit}, selectedCatalogGroup)  {
    commit('updateSelectedCatalogGroup' , selectedCatalogGroup)
  },

  UPDATE_CATALOGGROUP({commit}, dataToSend) {
    return wsapi.$axios.patch(`/cataloggroups/${dataToSend.catalogGroupId}`, dataToSend.newData )
      .then(response => {
        return response
      })
  }
}

// mutations
const mutations = {
    updateCatalogGroupList: (state, catalogGroupList) =>{
        state.catalogGroupList = catalogGroupList
    },
    updateSelectedCatalogGroup: (state, selectedCatalogGroup) => {
      state.selectedCatalogGroup.data = selectedCatalogGroup
    }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}