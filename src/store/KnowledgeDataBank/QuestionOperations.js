// state
const state = {
    selectedItem: {}
}

//  getters
const getters = {
    getSelectedItem(state) {
        return state.selectedItem
    }
}

// actions
const actions = {
    SELECT_ITEM( {commit}, selectedItem ) {
        commit('wsUpdateSelectedItem' , selectedItem)
    }
}

// mutations
const mutations = {
    wsUpdateSelectedItem: (state, selectedItem) => {
        state.selectedItem.data = selectedItem
    }
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}