import wsapi from  '../services/wsApi'
import router from '../router'
import CGOperations from './CatalogGroup/CGOperations'
import CSGOperations from './CatalogSubGroup/CSGOperations'
import User from './User/UserOperations'
import Admin from './Admin/AdminOperations'
import Catalog from './Catalog/Catalog'
import AdminGroup from './Group/AdminGroups/AdminGroupOperation'
import UserGroup from './Group/UserGroups/UserGroupOperations'
import UserGroupforUser from './Group/UserGroups/UserGroupOperationsForUser'
import AdminGroupforAdmin from './Group/AdminGroups/AdminGroupOperationsForAdmin'

const state = {
  selectedItem: {
    itemType: '',
    itemTittle: '',
    path: '',
    data: {},
  },
  groupList: [],
  activeTab: 0
}

// getters
const getters = {
  getSelectedItem(state) {
    return state.selectedItem
  },
  getActiveTab(state) {
    return state.activeTab
  },
  getGroupList(state) {
    return state.groupList
  }
}

// actions
const actions = {
    SELECT_ITEM_FOR_ROUTE({commit}, selectedItem) {
        commit('updateSelectedItem',selectedItem)
    },
    UPDATE_ACTIVE_TAB({commit}, activeTab) {
      commit('updateActiveTab', activeTab)
    },
    UPDATE_ITEM({commit}, inputData) {
      var dataToSend
      if(state.selectedItem.itemType === 'CATALOGGROUP') {
          dataToSend= {
          catalogGroupId : inputData.id,
          newData : {
              name : inputData.name,
              description : inputData.description
          }
        }
        return CGOperations.actions.UPDATE_CATALOGGROUP({commit}, dataToSend)

      }else if(state.selectedItem.itemType === 'CATALOGSUBGROUP') {
          dataToSend= {
          catalogGroupId : CGOperations.getters.getSelectedCatalogGroup(CGOperations.state).data.id,
          catalogSubgroupId : inputData.id,
          newData : {
              name : inputData.name,
              description : inputData.description
          }
        }
        return CSGOperations.actions.UPDATE_CATALOGSUBGROUP({commit}, dataToSend)

      }else if(state.selectedItem.itemType === 'CATALOG') {
        dataToSend = {
          catalogId : state.selectedItem.data.id,
          catalogGroupId : CGOperations.getters.getSelectedCatalogGroup(CGOperations.state).data.id,
          catalogSubgroupId: CSGOperations.getters.getSelectedCatalogSubGroup(CSGOperations.state).data.id,
          newData : {
              name : inputData.name,
              description : inputData.description
          }
        }
      return Catalog.actions.UPDATE_CATALOG({commit}, dataToSend)

    }else if(state.selectedItem.itemType === 'ADMINGROUP') {
      dataToSend = {
          id : inputData.id,
          data : {
              name : inputData.name,
              description : inputData.description
          }
      }
      return AdminGroup.actions.UPDATE_ADMINGROUP({commit}, dataToSend)

    }else if(state.selectedItem.itemType === 'USERGROUP') {
      dataToSend = {
          id : inputData.id,
          data : {
              name : inputData.name,
              description : inputData.description
          }
      }
      return UserGroup.actions.UPDATE_USERGROUP({commit}, dataToSend)

    }else if(state.selectedItem.itemType === 'USER') {
      return User.actions.UPDATE_USER({commit}, inputData.inputData)   // User will change
      .then( response => {
        return {
          message: response.data.message
        }
      })
      .then( responsUpdateUser => {
        if(inputData.passwordChange.currentPassword != "" || 
          inputData.passwordChange.newPassword !== "" || 
          inputData.passwordChange.confirmPassword != "") {

          dataToSend = {
            id : state.selectedItem.data.id,
            data : inputData.passwordChange
          }
          return User.actions.UPDATE_USER_PASSWORD({commit}, dataToSend)
          .then(respondUpdatePassword => {
            return {
              message: `${responsUpdateUser.message} und ${respondUpdatePassword.data.message}`
            }
          }) 
        }else {
          return responsUpdateUser
        }
      })
    }else if(state.selectedItem.itemType == 'ADMIN') {
      return Admin.actions.UPDATE_ADMIN({commit}, inputData.inputData)   // Admin will change
      .then( response => {
          return {
            message: response.data.message
          }
      })
      .then( responsUpdateAdmin => {
        if(inputData.passwordChange.currentPassword != "" || 
          inputData.passwordChange.newPassword !== "" || 
          inputData.passwordChange.confirmPassword != "") {
            
          dataToSend = {
            id : state.selectedItem.data.id,
            data : inputData.passwordChange
          }
          return Admin.actions.UPDATE_ADMIN_PASSWORD({commit}, dataToSend)
          .then(respondUpdatePassword => {
            console.log(respondUpdatePassword)
            return {
              message: `${responsUpdateAdmin.message} und ${respondUpdatePassword.data.message}`
            }
          }) 
        }else {
          return responsUpdateAdmin
        }
      })//close .then
    }
  }, //UPDATE ITEM
  
  ADD_ITEM({commit}, inputData) {
    var dataToSend
    if(state.selectedItem.itemType === 'CATALOGGROUP') {
      dataToSend= {
          name : inputData.name,
          description : inputData.description
      }
      return CGOperations.actions.ADD_CATALOGGROUP({commit}, dataToSend)
    }else if(state.selectedItem.itemType === 'CATALOGSUBGROUP') {
      dataToSend = {
          catalogGroupId: CGOperations.getters.getSelectedCatalogGroup(CGOperations.state).data.id,
          data: {
            name : inputData.name,
            description : inputData.description
          }
      }
      return CSGOperations.actions.ADD_CATALOGSUBGROUP({commit}, dataToSend)

    }else if(state.selectedItem.itemType === 'CATALOG') {
      dataToSend = {
          catalogGroupId: CGOperations.getters.getSelectedCatalogGroup(CGOperations.state).data.id,
          catalogSubgroupId: CSGOperations.getters.getSelectedCatalogSubGroup(CSGOperations.state).data.id,
          data: {
            name : inputData.name,
            description : inputData.description
          }
      }
      return Catalog.actions.ADD_CATALOG({commit}, dataToSend)

    }else if(state.selectedItem.itemType === 'ADMINGROUP') {
      return AdminGroup.actions.ADD_ADMINGROUP({commit}, inputData)
    }else if(state.selectedItem.itemType === 'USERGROUP') {
      return UserGroup.actions.ADD_USERGROUP({commit}, inputData)
    }else if(state.selectedItem.itemType === 'USER') {
      return User.actions.ADD_USER({commit}, inputData.inputData)   // User will change
      .then( response => {
        console.log(response)
          return response
      })
      .then( respondAddUser => {
        var UserToGroupMessage = ''
        if(inputData.selectedList.length != 0) {
          inputData.selectedList.forEach(userGroup => {
            dataToSend = {
              userGroupId : userGroup,
              data : {
                users: [respondAddUser.data.data.id]
              }
            }
            UserToGroupMessage = UserGroupforUser.actions.ADD_USER_LIST_TO_USERGROUP({commit}, dataToSend)
            .then(respondAddUserToGroup => {
              return respondAddUserToGroup.data.message
            })
          })
          return {
            message: `${respondAddUser.data.message} und ${UserToGroupMessage}`
          }
        }else {
          return {
              message : respondAddUser.data.message
          }
        }
      })

    }else if(state.selectedItem.itemType === 'ADMIN') {
      return Admin.actions.ADD_ADMIN({commit}, inputData.inputData)   // User will change
      .then( response => {
          return response
      })//close .then
      .then( respondAddAdmin => {
        var AdminToGroupMessage = ''
        if(inputData.selectedList.length != 0) {
          inputData.selectedList.forEach(adminGroup => {
            dataToSend = {
              adminGroupId : adminGroup,
              data : {
                admin: [respondAddAdmin.data.data.id]
              }
            }
            AdminToGroupMessage = AdminGroupforAdmin.actions.ADD_ADMIN_LIST_TO_ADMINGROUP({commit}, dataToSend)
            .then(respondAddAdminToGroup => {
              console.log(respondAddAdminToGroup.data.message)
              return respondAddAdminToGroup.data.message
            })
          })
          return {
            message: `${respondAddAdmin.data.message} und ${AdminToGroupMessage}`  
          }
        }else {
          return {
              message : respondAddAdmin.data.message
          }
        }
      })//close .then
    }
  },
  DELETE_ITEM({commit}) {
    var dataToSend
    if(state.selectedItem.itemType === 'CATALOGGROUP') {
      return CGOperations.actions.DELETE_CATALOGGROUP_BY_ID({commit}, state.selectedItem.data.id)
    }else if(state.selectedItem.itemType === 'CATALOGSUBGROUP') {
      dataToSend= {
        catalogSubgroupId : state.selectedItem.data.id,
        catalogGroupId : CGOperations.getters.getSelectedCatalogGroup(CGOperations.state).data.id
      }
      return CSGOperations.actions.DELETE_CATALOGSUBGROUP({commit}, dataToSend)
    }else if(state.selectedItem.itemType === 'CATALOG') {
      dataToSend= {
        catalogId : state.selectedItem.data.id,
        catalogGroupId : CGOperations.getters.getSelectedCatalogGroup(CGOperations.state).data.id,
        catalogSubgroupId: CSGOperations.getters.getSelectedCatalogSubgroup(CSGOperations.state).data.id
      }
      return Catalog.actions.DELETE_CATALOG({commit}, dataToSend)
    }else if(state.selectedItem.itemType === 'ADMINGROUP') {
      return AdminGroup.actions.DELETE_ADMINGROUP_BY_ID({commit}, AdminGroup.getters.getSelectedAdminGroup(AdminGroup.state).data.id)
    }else if(state.selectedItem.itemType === 'USERGROUP') {
      return UserGroup.actions.DELETE_USERGROUP_BY_ID({commit}, UserGroup.getters.getSelectedUserGroup(UserGroup.state).data.id)
    }else if(state.selectedItem.itemType === 'USER') {
      return User.actions.DELETE_USER_BY_ID({commit}, User.getters.getSelectedUser(User.state).data.id)
    }else if(state.selectedItem.itemType === 'ADMIN') {
      return Admin.actions.DELETE_ADMIN_BY_ID({commit}, Admin.getters.getSelectedAdmin(Admin.state).data.id)
    }
  },
  GET_GROUPS({commit}) {
    if(state.selectedItem.itemType === "USER") {
      return UserGroup.actions.GET_USERGROUP_LIST({commit})
      .then( response => {
        commit('updateGroupList',response.data.data.groups)
        return response
      })
    }else if(state.selectedItem.itemType === "ADMIN") {
      return AdminGroup.actions.GET_ADMINGROUP_LIST({commit})
      .then( response => {
        commit('updateGroupList',response.data.data.groups)
        return response
      })
    }
  }
}

// mutations
const mutations = {
  updateActiveTab: (state,activeTab) => {
    state.activeTab = activeTab
  },
  updateSelectedItem: (state, selectedItem)  => {
    state.selectedItem = selectedItem
  },
  updateGroupList: (state, groupList) => {
    state.groupList = groupList
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}