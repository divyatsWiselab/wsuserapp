import wsapi from  '../../services/wsApi'

const state = {
    selectedLanguage: {shortLang: "de", longLang: "Deutsch"}
}

// getters
const getters = {
    selectedLanguage(state) {
        return state.selectedLanguage
    }
}
// actions

// mutations

export default {
    namespaced: true,
    state,
    getters
}