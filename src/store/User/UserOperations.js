import wsapi from  '../../services/wsApi'
 

const state = {
  usersList: [],
  deletedUserList: [],
  selectedUser: {
    itemType: "USER",
    itemTittle: "Benutzer",
    path: "/User",
    data: {}
  }
}

// getters
const getters = {
  getUserList(state){
    return state.usersList
  },
  getSelectedUser(state) {
    return state.selectedUser
  },
  getDeletedUserList(state) {
    return state.deletedUserList
  }
}

// actions
const actions = {
    ADD_USER (commit , userData) {
        return wsapi.$axios.post('/users', userData)
          .then(response => {     
            return response
        })
    },

    GET_USER_LIST({ commit }) {
        return wsapi.$axios.get('users?page=0&size=1000&sort=createdAt,asc')
          .then(response => {
            commit('updateUserList' , response.data.data.users)
            return  response
          })
    },
    GET_DELETED_USER_LIST({commit}) {
      return wsapi.$axios.get('users?page=0&size=1000&sort=createdAt&delated=true,asc')
      .then(response => {
        commit('updateDeletedUser' , response.data.data.users)
        return  response
      })
    },
    DELETE_USER_BY_ID({commit} , userId)  {
      return wsapi.$axios.delete(`users/${userId}`)
        .then(response => {
          return response
        })
    },
    
    GET_SINGLE_USER_BY_ID ({commit}, userId)  {
      return wsapi.$axios.get(`/users/${userId}`)
        .then(response => {
          commit('updateUserList',response.data.data)
          return response
        })
    },
    
    UPDATE_USER({commit} , dataToSend) {
      return wsapi.$axios.patch(`/users/${dataToSend.id}`, dataToSend)
        .then(response => {
          return response
        })
    },

    UPDATE_USER_PASSWORD({commit}, dataToSend) {
      return wsapi.$axios.patch(`/users/${dataToSend.id}/password`, dataToSend.data)
      .then(response => {
        return response
      })
    },

    SELECT_USER( {commit}, selectedUser)  {
      commit('updateSelectedUser' , selectedUser)
    }
}

// mutations
const mutations = {
  updateUserList: (state, usersList)  => {
    state.usersList = usersList
  },
  updateSelectedUser: (state, selectedUser) => {
    state.selectedUser.data = selectedUser
  },
  updateDeletedUser: (state, deletedUserList) => {
    state.deletedUserList = deletedUserList
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}