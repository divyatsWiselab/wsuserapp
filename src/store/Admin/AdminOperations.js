import wsapi from  '../../services/wsApi'
 

const state = {
    adminList: [],
    selectedAdmin: {
      itemType: "ADMIN",
      itemTittle: "Admin",
      path: "/User",
      data: {}
    }
}

// getters
const getters = {
    getAdminList(state){
      return state.adminList
    },
    getSelectedAdmin(state) {
      return state.selectedAdmin
    }
}

// actions
const actions = {
  ADD_ADMIN ({commit}, adminObject) {
    return wsapi.$axios.post('admins', adminObject)
    .then(response => {
      return response
    })
  },
  
  DELETE_ADMIN_BY_ID  ({commit}, adminId)  {
    return wsapi.$axios.delete(`admins/${adminId}`)
    .then(response => {
      return response
    })
  },
  
   GET_ADMIN_LIST ({commit}) { 
    var pagesize = '100'
    return wsapi.$axios.get(`admins?page=0&size=${pagesize}`)
      .then(response => { 
        commit('updateAdminList' , response.data.data.admins)
        return response
      })
  },
  
   GET_ADMIN_BY_ID({commit}, adminId)  {
    return wsapi.$axios.get(`/admins/${adminId.toString()}`)
    .then(response => {
      return response
    })
  },
  
  UPDATE_ADMIN  ({commit}, dataToSend) {
    return wsapi.$axios.patch(`/admins/${dataToSend.id}`, dataToSend )
      .then(response => {
        return response
      })
  },

  UPDATE_ADMIN_PASSWORD({commit}, dataToSend) {
    return wsapi.$axios.patch(`/admins/${dataToSend.id}/password`, dataToSend.data)
    .then(response => {
      return response
    })
  },

  SELECT_ADMIN( {commit}, selectedAdmin)  {
    commit('updateSelectedAdmin' , selectedAdmin)
  }
}

// mutations
const mutations = {  
  updateAdminList : (state, adminList) =>{
    state.adminList = adminList
  },
  updateSelectedAdmin: (state, selectedAdmin) => {
    state.selectedAdmin.data = selectedAdmin
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}