import wsapi from  '../../../services/wsApi'
 

const state = {
  adminPermissionList: [],
}

const baseURL = wsapi.$axios.defaults.baseURL

// getters
const getters = {
  getAdminPermissionList(state){
    return state.adminPermissionList
  }
}

// actions
const actions = {
    ADD_CATALOGGROUP_PERMISSION_TO_ADMIN ({commit}, dataToSend) {
        return wsapi.$axios.post(`/admins/${dataToSend.adminId}/cataloggroups/${dataToSend.catalogGroupId}/permissions`, dataToSend.data)
        .then(response => {      
              return response
        })
    },
    ADD_CATALOGSUBGROUP_PERMISSION_TO_ADMIN ({commit},dataToSend) {
      return wsapi.$axios.post(`/admins/${dataToSend.adminId}/cataloggroups/${dataToSend.catalogGroupId}/subgroups/${dataToSend.catalogSubgroupId}/permissions`, dataToSend.data)
      .then(response => {      
            return response
      })
  },
    GET_ALL_SYSTEM_PERMISSIONS({ commit }) {
        return wsapi.$axios.get('/permissions/')
          .then(response => {
            commit('updateAdminPermissionList' , response.data.data.permissions)
            return  response
          })
    },
    GET_ADMINPERMISSION_LIST_BY_ID({ commit }, adminId) {
      return wsapi.$axios.get(`admins/${adminId}/authoringpermissions/`)
        .then(response => {
          var permissions = response.data.data.catalogGroups
            .flatMap( catalogGroup => catalogGroup.catalogSubGroups)
            .flatMap( catalogSubgroup => {
              return {
               name: catalogSubgroup.name,
               permission: catalogSubgroup.permissions
              }
            })

          permissions.push.apply(permissions,
            response.data.data.catalogGroups
            .flatMap(catalogGroup => {
              return {
                name: catalogGroup.name,
                description: catalogGroup.description,
                permission: catalogGroup.permissions
              }
            })
          )
          commit('updateAdminPermissionList' , permissions)
          return  response
        })
    },
  //   GET_ADMINPERMISSION_BY_LINK({ commit }, link) {
  //     if(link.startsWith(baseURL)) {
  //       return wsapi.$axios.get(link.replace(baseURL,''))
  //       .then(response => {
  //         commit('updateAdminPermissionList' , response.data.data.permissions)
  //         console.log(response)
  //         return  response
  //       })
  //     }
  // },
    GET_ADMIN_CATALOGGROUPPERMISSION_LIST ({commit}, dataToSend)  {
      return wsapi.$axios.get(`/admins/${dataToSend.adminId}/cataloggroups/${dataToSend.catalogGroupId}/permissions/`)
        .then(response => {
          commit('updateAdminPermissionList',response.data.data.permissions)
          return response
        })
    },
    GET_ADMIN_CATALOGSUBGROUPPERMISSION_LIST ({commit}, dataToSend)  {
      return wsapi.$axios.get(`/admins/${dataToSend.adminId}/cataloggroups/${dataToSend.catalogGroupId}/subgroups/${dataToSend.catalogSubgroupId}/permissions/`)
        .then(response => {
          commit('updateAdminPermissionList',response.data.data.permissions)
          return response
        })
    }
}

// mutations
const mutations = {
  updateAdminPermissionList: (state, permissionList)  => {
    state.adminPermissionList = permissionList
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}