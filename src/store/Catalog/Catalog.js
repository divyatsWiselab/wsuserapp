import wsapi from  '../../services/wsApi'
 

const state = {
    catalogList: [],
    selectedCatalog: {
    itemType: "CATALOG",
    itemTittle: "Kataloge",
    path: "/Authorentool_Catalog",
    data: {}
    }
}

// getters
const getters = {
    getCatalogList(state){
      return state.catalogList
    },
    getSelectedCatalog(state) {
      return state.selectedCatalog
    }
}

// actions
const actions = {
  
 ADD_CATALOG (commit, dataToSend)  {
    return wsapi.$axios.post(`cataloggroups/${dataToSend.catalogGroupId}/subgroups/${dataToSend.catalogSubgroupId}/catalogs`, dataToSend.data)
    .then(response => {
      return response
    })
},

 DELETE_CATALOG  (commit, dataToSend) {
    return wsapi.$axios.delete(`cataloggroups/${dataToSend.catalogGroupId}/subgroups/${dataToSend.catalogSubgroupId}/catalogs/${dataToSend.catalogId}`)
    .then(response => {
         return response
    })
},

 GET_SINGLE_CATALOG  ({commit} , dataToSend)  {
    return wsapi.$axios.get(`cataloggroups/${dataToSend.catalogGroupId}/subgroups/${dataToSend.catalogSubgroupId}/catalogs/${dataToSend.catalogId.toString()}`)
    .then(response => {
        commit('updateCatalogList' , response.data.data.catalogs[0])
        return response
    })
},

  GET_CATALOG_LIST ({commit}, dataToSend)  {
    return wsapi.$axios.get(`cataloggroups/${dataToSend.catalogGroupId}/subgroups/${dataToSend.catalogSubgroupId}/catalogs`)
    .then(response => {
        commit('updateCatalogList' , response.data.data.catalogs)
        return response
    })
  },  

  SELECT_CATALOG ({commit}, selectedCatalog)  {
    commit('updateSelectedCatalog',selectedCatalog)
  },

 UPDATE_CATALOG ({commit}, dataToSend)  {
    return wsapi.$axios.patch(`cataloggroups/${dataToSend.catalogGroupId}/subgroups/${dataToSend.catalogSubgroupId}/catalogs/${dataToSend.catalogId}`, dataToSend.newData )
    .then(response => {
        return response
    })
}

}

// mutations
const mutations = {
  updateCatalogList: (state, catalogList) =>{
      state.catalogList = catalogList
  },
  updateSelectedCatalog: (state, selectedCatalog) =>{
    state.selectedCatalog.data = selectedCatalog
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}