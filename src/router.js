import Vue from 'vue'
import Router from 'vue-router'
import Login from './views/Login.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'login',
      component: Login
    },
    {
      path: '/Login',      
      component: Login
    },
    {
      path: '/LandingPage',      
      name: 'LandingPage',
      component: () => import('./views/LandingPage.vue'),
      children: [
        {
          // A will be rendered in the second <router-view>
          // when /your-sidebar-url/a is matched
          path: '/Dashboard',
          components: {
            LandingPageContent:() => import('./views/Dashboard.vue')
          }
        },
        {
          path: '/AppDownload',
          components: {            
            LandingPageContent:() => import( './views/AppDownload.vue')
            }
        },
        {
          path: '/Impressum',
          components: { LandingPageContent: () => import( './views/Impressum.vue')}
        },
//////////////////////////
//
//AUTOREN PERMISSION
//
//////////////////////////
        {
          path: '/Permission_Autoren',
          components: { LandingPageContent: () => import( './views/Permissions/Autoren/Autoren_Admin.vue')}
        },
        {
          path: '/Permission_Autoren_CatalogGroup/',
          components: { LandingPageContent: () => import( './views/Permissions/Autoren/Autoren_CatalogGroup.vue')}
        },
        {
          path: '/Permission_Autoren_CatalogSubGroup/',
          components: { LandingPageContent: () => import( './views/Permissions/Autoren/Autoren_CatalogSubGroup.vue')}
        },
//////////////////////////
//
//QUESTION CATALOG
//
//////////////////////////
        {
          path: '/Authorentool/:catalogGroupId/:catalogSubgroupId/:catalogId',
          components: { LandingPageContent: () => import( './views/Authorentool/Authorentool.vue')}
        },
//////////////////////////
//
//AUTHORENTOOL - CATALOG GROUP
//
//////////////////////////
        {
          path: '/Authorentool_CatalogGroup/',
          components: { LandingPageContent: () => import( './views/Authorentool/Authorentool_CatalogGroup.vue')}
        },
        //with return message
        {
          path: '/Authorentool_CatalogGroup/:message',
          components: { LandingPageContent: () => import( './views/Authorentool/Authorentool_CatalogGroup.vue')}
        },
        {
          path: '/AddCatalogGroup/',
          components: { LandingPageContent: () => import( './Components/AddItem/AddItemV1Comp.vue')}
        },
        {
          path: '/UpdateCatalogGroup/',
          components: { LandingPageContent: () => import( './Components/UpdateItem/UpdateItemV1Comp.vue')}
        },
        {
          path: '/DeleteCatalogGroup/',
          components: { LandingPageContent: () => import('./Components/DeleteConfirmationComp.vue')}
        },
//////////////////////////
//
//AUTHORENTOOL - CATALOG SUBGROUP
//
//////////////////////////
        {
          path: '/Authorentool_CatalogSubgroup/',
          components: { LandingPageContent: () => import( './views/Authorentool/Authorentool_CatalogSubgroup.vue')}
        },
        //with return message
        {
          path: '/Authorentool_CatalogSubgroup/:message',
          components: { LandingPageContent: () => import( './views/Authorentool/Authorentool_CatalogSubgroup.vue')}
        },
        {
          path: '/AddCatalogSubgroup/',
          components: { LandingPageContent: () => import('./Components/AddItem/AddItemV1Comp.vue')}
        },
        {
          path: '/UpdateCatalogSubgroup/',
          components: { LandingPageContent: () => import( './Components/UpdateItem/UpdateItemV1Comp.vue')}
        },
        {
          path: '/DeleteCatalogSubgroup/',
          components: { LandingPageContent: () => import('./Components/DeleteConfirmationComp.vue')}
        },
//////////////////////////
//
//AUTHORENTOOL - CATALOG
//
//////////////////////////
        {
          path: '/Authorentool_Catalog/',
          components: { LandingPageContent: () => import( './views/Authorentool/Authorentool_Catalog.vue')}
        },
        //with return message
        {
          path: '/Authorentool_Catalog/:message',
          components: { LandingPageContent: () => import( './views/Authorentool/Authorentool_Catalog.vue')}
        },
        {
          path: '/AddCatalog/',
          components: { LandingPageContent: () => import('./Components/AddItem/AddItemV1Comp.vue')}
        },
        {
          path: '/UpdateCatalog/',
          components: { LandingPageContent: () => import( './Components/UpdateItem/UpdateItemV1Comp.vue')}
        },
        {
          path: '/DeleteCatalog/',
          components: { LandingPageContent: () => import('./Components/DeleteConfirmationComp.vue')}
        },
//////////////////////////
//

//
//////////////////////////
        {
          path: '/Plantool',
          components: { LandingPageContent: () => import( './views/PlanTool.vue')}
        },
        {
          path: '/Support',
          components: { LandingPageContent: () => import( './views/Support.vue')}
        },
        {
          path: '/ControlPanel',
          components: { LandingPageContent: () => import( './views/ControlPanel.vue')}
        },
/////////////////
//
//USER
//
/////////////////
        {
          path: '/User',
          components: { LandingPageContent: () => import( './views/User/User.vue')}
        },
        {
          path: '/User/:message',
          components: { LandingPageContent: () => import( './views/User/User.vue')}
        },
        {
          path: '/AddUser/',
          components: { LandingPageContent: () => import('./Components/AddItem/AddItemV2Comp.vue')}
        },
        {
          path: '/UpdateUser/',          
          components:   { LandingPageContent: () => import('./Components/UpdateItem/UpdateItemV2Comp.vue')}
        },
        {
          path: '/DeleteUser/',   
          components:   { LandingPageContent: () => import( './Components/DeleteConfirmationComp.vue')}
        },
/////////////////
//
//ADMIN GROUP//
//
/////////////////

        {
          path: '/AdminGroup',
          components: { LandingPageContent: () => import('./views/Group/AdminGroup.vue')}
        },
        //with message
        {
          path: '/AdminGroup/:message',
          components: { LandingPageContent: () => import('./views/Group/AdminGroup.vue')}
        },
        {
          path: '/AddAdminGroup/',
          components: { LandingPageContent: () => import('./Components/AddItem/AddItemV1Comp.vue')}
        },
        {
          path: '/UpdateAdminGroup/',          
          components:   { LandingPageContent: () => import('./Components/UpdateItem/UpdateItemV1Comp.vue')}
        },
        {
          path: '/DeleteAdminGroup/',   
          components:   { LandingPageContent: () => import( './Components/DeleteConfirmationComp.vue')}
        },
        //add admin to group
        {
          path: '/AddAdminToGroup',   
          components:   { LandingPageContent: () => import( './views/Group/AddAdminToGroup.vue')}
        },
/////////////////
//
//USER GROUP// 
//
////////////////       
        {
          path: '/UserGroup',
          components: { LandingPageContent: () => import('./views/Group/UserGroup.vue')}
        },
        //with message
        {
          path: '/UserGroup/:message',
          components: { LandingPageContent: () => import('./views/Group/UserGroup.vue')}
        },
        {
          path: '/AddUserGroup',
          components: { LandingPageContent: () => import('./Components/AddItem/AddItemV1Comp.vue')}
        },
        {
          path: '/UpdateUserGroup/',          
          components:   { LandingPageContent: () => import( './Components/UpdateItem/UpdateItemV1Comp.vue')}
        },
        {
          path: '/DeleteUserGroup/',   
          components:   { LandingPageContent: () => import( './Components/DeleteConfirmationComp.vue')}
        },
        //add user to group
        {
          path: '/AddUserToGroup',   
          components:   { LandingPageContent: () => import( './views/Group/AddUserToGroup.vue')}
        },

/////////////////
//
//ADMIN
//
////////////////
        {
          path: '/Admin',
          components: { LandingPageContent: () => import( './views/Admin/Admin.vue')}
        },
        //with message
        {
        path: '/Admin/:message',
        components: { LandingPageContent: () => import('./views/Admin/Admin.vue')}
        },
        {
        path: '/AddAdmin/',
        components: { LandingPageContent: () => import('./Components/AddItem/AddItemV2Comp.vue')}
        },
        {
        path: '/UpdateAdmin/',          
        components:   { LandingPageContent: () => import( './Components/UpdateItem/UpdateItemV2Comp.vue')}
        },
        {
        path: '/DeleteAdmin/',   
        components:   { LandingPageContent: () => import( './Components/DeleteConfirmationComp.vue')}
        },
////////////////
//
//
////////////////
        {
          path: '/Selectbox',
          components: { LandingPageContent: () => import('./views/WiselabSelectBox.vue')}
        },
////////////////
//
// SYSTEMSETTINGS
//
////////////////
        {
          path: '/System_Settings',
          components: { LandingPageContent: () => import('./views/SystemSettings/SystemSettings.vue')} 
        },
////////////////
//
// KNOWLEDGEDATABANK
//
////////////////
        {
          path: '/MyQuestions',
          components: { LandingPageContent: () => import('./views/KnowledgeDataBank/MyQuestions.vue') }
        },
        {
          path: '/OpenQuestions',
          components: { LandingPageContent: () => import('./views/KnowledgeDataBank/OpenQuestions.vue') }
        },
        {
          path: '/Questions',
          components: { LandingPageContent: () => import('./views/KnowledgeDataBank/Questions.vue') }
        },
        {
          path: '/UpdateQuestion',
          components: { LandingPageContent: () => import('./Components/UpdateItem/UpdateItemV3Comp.vue') }
        },

      ]
    },

    
    //////////////////////////
    //
    // User App router --starts
    //
    //////////////////////////
    {
      path: '/UserLandingPage',      
      name: 'UserLandingPage',
      component: () => import('./views/UserApp/UserLandingPage.vue'),
      children: [
        {
          path: '/UserDashboard',
          components: {
            UserLandingPageContent:() => import('./views/UserApp/UserDashboard.vue')
          }
        },
        {
          path: '/UserTasks',
          components: {
            UserLandingPageContent:() => import('./views/UserApp/UserTasks.vue')
          }
        },
        {
          path: '/UserArchive',
          components: {
            UserLandingPageContent:() => import('./views/UserApp/UserArchive.vue')
          }
        },
        {
          path: '/UserWiki',
          components: {
            UserLandingPageContent:() => import('./views/UserApp/UserWiki.vue')
          }
        },
        {
          path: '/UserChangePassword',
          components: {
            UserLandingPageContent:() => import('./views/UserApp/UserChangePassword.vue')
          }
        },
        {
          path: '/UserImpressum',
          components: {
            UserLandingPageContent:() => import('./views/UserApp/UserImpressum.vue')
          }
        },
      ]
    }
    // User App router --ends

    // routers witout6
  ]
})
